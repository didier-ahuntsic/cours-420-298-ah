import rawData from "https://didier-ahuntsic.gitlab.io/cours-420-317-ah/data/musee_v02.json" assert { type: "json" };

// Define data types
type Visitor = {
    name: string;
    id: number;
};
type Event = {
    roomId: number;
    time: string;
    visitorId: number;
};
type RawData = {
    visitors: Visitor[];
    events: Event[];
};

// Function to convert time strings to numbers
function tempsToNumber(temps: string): number {
    const [hours, minutes] = temps.split(":").map(Number);
    return hours * 60 + minutes;
}

// Function to order events by time

function eventTimeSortFunction(a: Event, b: Event) {
    return tempsToNumber(a.time) - tempsToNumber(b.time);
}
function orderEventByTime(events: Event[]): Event[] {
    return events.slice().sort(eventTimeSortFunction);
}

// Function to get the opening hour
function getOpeningHour(rawData: RawData): string {
    const sortedEvents = orderEventByTime(rawData.events);
    return sortedEvents[0].time;
}

// Function to get the closing hour
function getClosingHour(rawData: RawData): string {
    const sortedEvents = orderEventByTime(rawData.events);
    return sortedEvents[sortedEvents.length - 1].time;
}

// Function to count visitors
function countVisitors(rawData: RawData): number {
    return rawData.visitors.length;
}


function listVisitorsOfRoomID(rawData: RawData, roomIDToAnalyse: number): string[] {
    const visitorNames: string[] = [];

    for (const event of rawData.events) {
        if (event.roomId === roomIDToAnalyse) {
            // Find the visitor's name based on their visitorId
            const visitor = rawData.visitors.find((v) => v.id === event.visitorId);
            if (visitor) {
                visitorNames.push(visitor.name);
            }
        }
    }

    return visitorNames;
}

function maxRoomID(rawData: RawData): number {
    const roomVisitors: number[] = [];
    for (const event of rawData.events) {
        roomVisitors.push(event.roomId);
    }
    const maxRoomID = Math.max(...roomVisitors);
    return maxRoomID;
}
function minRoomID(rawData: RawData): number {
    const roomVisitors: number[] = [];
    for (const event of rawData.events) {
        roomVisitors.push(event.roomId);
    }
    const maxRoomID = Math.min(...roomVisitors);
    return maxRoomID;
}

function findId(rawData: RawData, name: string): number {
    for (const visitor of rawData.visitors) {
        if (visitor.name === name) {
            return visitor.id;
        }
    }
    return -1;
}

function pathOfVisitor(visitorId:number, rawData: RawData): number[] {
    const path: number[] = [];
    for (const event of rawData.events) {
        if (event.visitorId === visitorId) {
            path.push(event.roomId);
        }
    }
    return path;
}



function findFirstAndLastRooms(rawData: RawData): [number, number] {
    const sortedEvents = orderEventByTime(rawData.events);
    const firstRoom = sortedEvents[0].roomId;
    console.log(sortedEvents)
    const lastRoom = sortedEvents.reverse()[0].roomId;

    return [firstRoom, lastRoom];

}

const firstRoomVisited = findFirstAndLastRooms(rawData)[0];
const lastRoomVisited = findFirstAndLastRooms(rawData)[1];

console.log(`The first room visited was ${firstRoomVisited}.`);
console.log(`The last room visited was ${lastRoomVisited}.`);
function mostActiveVisitor(rawData: RawData): Visitor | null {
    let mostActive: Visitor | null = null;
    let maxRoomsVisited = 0;

    for (const visitor of rawData.visitors) {
        const roomsVisited = pathOfVisitor(visitor.id, rawData).length;

        if (roomsVisited > maxRoomsVisited) {
            mostActive = visitor;
            maxRoomsVisited = roomsVisited;
        }
    }

    return mostActive;
}



const roomIDToAnalyse = 3;
const roomIDToCount = 3;


//         2a
console.log(`Opening Hour: ${getOpeningHour(rawData)}`);
console.log(`Closing Hour: ${getClosingHour(rawData)}`);

//        2b
console.log(`Total number of visitors: ${countVisitors(rawData)}`);

//          3a
// const visitorsRoom3:string[] = listVisitorsOfRoomID(rawData,3);
// console.log(`Number of visitors in room ${roomIDToCount}: ${visitorsRoom3.length}`);

// 3b
const VisitorNames:string[] = listVisitorsOfRoomID(rawData,3);
// console.log(visitor)
// console.log(`All visitor names: ${console.log(VisitorNames)}`);
// console.log(`The visitors in room ${roomIDToAnalyse} are ${VisitorNames}`);

// 3c
// const mostPopularRoom = maxRoomID(rawData);

//  3c

// 3c
console.log((maxRoomID(rawData)));
console.log((minRoomID(rawData)));

// 4a
const id:number = findId(rawData, "Jessica Daniel");
console.log(pathOfVisitor(id,rawData).toLocaleString());

//  4b

// 4c
console.log(mostActiveVisitor(rawData));


// console.log(pathOfVisitor(id, rawData));
//
// const [firstRoomVisited, lastRoomVisited] = findFirstAndLastRooms(rawData);
// console.log(`The first room visited was ${firstRoomVisited}.`);
// console.log(`The last room visited was ${lastRoomVisited}.`);
//5a
function averageRoomsVisited(rawData: RawData): number {
    const visitorRoomsCount: number[] = [];

    for (const visitor of rawData.visitors) {
        const roomsVisited = new Set<number>();

        for (const event of rawData.events) {
            if (event.visitorId === visitor.id) {
                roomsVisited.add(event.roomId);
            }
        }

        visitorRoomsCount.push(roomsVisited.size);
    }

    const totalRoomsVisited = visitorRoomsCount.reduce((sum, count) => sum + count, 0);
    const averageRooms = totalRoomsVisited / rawData.visitors.length;

    return averageRooms;
}

function averageTimeSpent(rawData: RawData): number {
    const visitorTimeSpent: number[] = [];

    for (const visitor of rawData.visitors) {
        let totalTimeSpent = 0;

        for (const event of rawData.events) {
            if (event.visitorId === visitor.id) {
                totalTimeSpent += tempsToNumber(event.time);
            }
        }

        visitorTimeSpent.push(totalTimeSpent);
    }

    const totalMinutesSpent = visitorTimeSpent.reduce((sum, time) => sum + time, 0);
    const averageMinutes = totalMinutesSpent / rawData.visitors.length;

    return averageMinutes;
}

const avgRoomsVisited = averageRoomsVisited(rawData);
console.log(`Average number of rooms visited by a visitor: ${avgRoomsVisited.toFixed(2)}`);

const avgTimeSpent = averageTimeSpent(rawData);
console.log(`Average time spent by a visitor (in minutes): ${avgTimeSpent.toFixed(2)}`);

// 4c
function visitorWithMostRoomsVisited(rawData: RawData): string | undefined {
    let maxVisitedRooms = 0;
    let visitorWithMostRooms: string | undefined;

    for (const visitor of rawData.visitors) {
        const roomsVisited = new Set<number>();

        for (const event of rawData.events) {
            if (event.visitorId === visitor.id) {
                roomsVisited.add(event.roomId);
            }
        }

        if (roomsVisited.size > maxVisitedRooms) {
            maxVisitedRooms = roomsVisited.size;
            visitorWithMostRooms = visitor.name;
        }
    }

    return visitorWithMostRooms;
}

const visitorName = visitorWithMostRoomsVisited(rawData);
console.log(`Le visiteur ayant visité le plus de salles est : ${visitorName}`);

// 5a
function findEntranceRooms(rawData: RawData): number[] {
    const entranceRooms: number[] = [];

    for (const event of rawData.events) {
        if (!entranceRooms.includes(event.roomID)) {
            entranceRooms.push(event.roomID);
        }
    }

    return entranceRooms;
}

const findEntranceRooms: number[] = areRoomsAdjacent(rawData);
console.log(`Les entrees du musee sont les salles ${isAdjacentTo8}`);
// 5b
function findExitRooms(rawData: RawData): number[] {
    const exitRooms: number[] = [];

    for (const event of rawData.events) {
        if (!exitRooms.includes(event.roomID)) {
            exitRooms.push(event.roomID);
        }
    }

    return exitRooms;
}

const findExitRooms: number[] = areRoomsAdjacent(rawData);
console.log(`Les sorties du musee sont les salles ${isAdjacentTo8}`);

// 5c
function areRoomsAdjacent(rawData: RawData, room1: number, room2: number): boolean {
    for (const event of rawData.events) {
        if (event.roomID === room1) {
            const nextEvent = rawData.events.find((e) => e.visitorId === event.visitorId && e.roomID === room2);
            if (nextEvent) {
                return true;
            }
        }
    }
    return false;
}
const isAdjacentTo8 = areRoomsAdjacent(rawData, 5, 8);
console.log(`Salle 5 est-elle adjacente à la salle 8 ? ${isAdjacentTo8}`);

// 5d


