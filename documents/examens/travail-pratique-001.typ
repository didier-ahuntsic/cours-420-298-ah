#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique I | Site Web Statique", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}
// #set heading(numbering: numbering-function, level: 1)



= Énoncé

Le but de cet exercice est de créer un nouveau site web pour 
les notes de cours. Vous trouverez plus bas trois exemple du
fait avec le site présent.

L'objectif de ce travail est d'amélioré le site web présentement
utilisé.  Le site web est très limité.

- Il ne peut y avoir plus d'une feuille d'exercice par cours
- On ne peut attacher de la documentation à un cours
- Il n'y a pas de pondération ni de date d'échéance aux examens.
- Le site est généralement laid.


== Exigences

- La page web doit être capable de montré jusqu'à 25 cours.

  - Le titre du cours doit être clairement visible

  - Chaque présentation doit être visualisable en mode présentation, 
    en mode html simple et en mode imprimable.

  - Il doit possible d'attacher plusieurs feuille d'exercices à un cours 

  - Il doit être possible d'attacher plusieurs feuilles de notes à un cours 

- La page web doit être capable de montré jusqu'à 5 travaux pratiques
  
  - Le titre du travail pratique doit être clairement visible 
  - La date d'échéance du travail pratique doit être clairement visible 
  - La pondération du travail pratique doit être indiqué 


- Lien github

   - Un lien github doit être clairement visible.


- Colophon.  L'auteur du site web doit être capable de "bragger" tel une caricature d'un 
  douchebag de laval à propose de:
   
  - La connaissance du mot "Colophon". 
  - La date de la mise-à-jour du site web 
  - Le git commit qui à produit le site web
  - L'URL pour trouver le code source du site web



== Exemple de site existant

- https://didiercrunch.gitlab.io/cours-420-306-ah/

- https://didier-ahuntsic.gitlab.io/cours-420-317-ah/

- https://cours-420-298-ah-didier-ahuntsic-fb35459a84540e8fa8fc3e9d7eb738.gitlab.io/



= Instructions

- Ce premier devoir doit être remis pour le 20 octobre 2023 à 23h59.
- Le  travail doit être fait individuellement.
- Le travail doit être remis sous la forme d'un projet 
  compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Le travail doit contenir un petit README avec votre nom.
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.

== Évaluation

- #points(none, 5, points-state) Utilisation d'une base html solide (scaffolding)
- #points(none, 10, points-state) Utilisation correct du html (balises table, list, div, span, ...)
- #points(none, 10, points-state) Utilisation correct du css
- #points(none, 10, points-state) Utilisation correct des classes et ids
- #points(none, 5, points-state)  Élégance et lisibilité du code
- #points(none, 20, points-state) Répondre aux critères du problèmes
- #points(none, 10, points-state) Beauté du résultats

== Quoi remettre

Le dossier compréssé à remettre doit comprendre un README où vous écriverez
votre nom.  Tout le code doit se trouver dans un sous dossier `src`.  La 
page web principale doit se nommer `index.html`.  Ici est un exemple du résultat.

```
README.md
src/
├─ index.html
├─ style.css

```

== Bonne chance
Si vous avez des question, n'hésite pas à contacter votre enseignant au didier.amyot\@collegeahuntsic.qc.ca ou par mio.


<end>