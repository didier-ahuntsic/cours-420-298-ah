#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique III | Lists, Objets et Itérations", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


= Instructions

- Ce troisième devoir doit être remis pour le 15 novembre 2023 à 23h59.
- Le  travail doit être fait individuellement.
- Le travail doit être remis sous la forme d'un projet 
  compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Le travail doit contenir un petit README avec votre nom (5% sera enlevé sinon).
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.


== Quoi remettre

Le dossier compréssé à remettre doit comprendre un README où vous écriverez
votre nom.  Tout le code doit se trouver dans un sous dossier `src`.  La 
page web principale doit se nommer `index.html`.  Ici est un exemple du résultat.

```
README.md
src/
├─ index.html
├─ script.js

```


== Bonne chance
Si vous avez des question, n'hésite pas à contacter votre enseignant au didier.amyot\@collegeahuntsic.qc.ca ou par mio.


= Énoncé

#rect(width: 100%, stroke: black)[
=== Truc

1. Lire et comprendre la question du point de vue de l'utilisateur.
2. Mettre la page web dans l'état attendu juste avant l'action.
3. Ouvrir l'inspecteur de chrome et essayer de faire l'exercice dans la console JavaScript.
4. Extraire la logique dans une fonction.
5. Mettre la fonction dans le fichier JavaScript (`script.js`).
6. Linker la fonction et le html avec un event approprié (`onclick`)
]


=== Exemples

Une implémentation du devoir est accessible au https://didier-ahuntsic.gitlab.io/cours-420-298-ah/examples/tp3.html.
Vous pouvez utiliser cette implémentation pour vous assurer de bien comprendre les questions.




== Lists, Objets et Itérations

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/tp3.html et modifier le code source.

=== Données
Certains numéros de cette question requièrent d'avoir accès aux données ci-bas.
==== Villes 
La liste à utiliser pour les villes est accessible à l'URL suivant.
https://gitlab.com/didier-ahuntsic/cours-420-298-ah/-/blob/main/cours/bases/villes.json



==== Personnes 
La liste à utiliser pour les personnes est accessible à l'URL suivant.
https://gitlab.com/didier-ahuntsic/cours-420-298-ah/-/blob/main/cours/bases/personnes.json


#set heading(numbering: numbering-function, level: 1)


=== Linker un script extérieur

#points(none, 1, points-state) 

Ajourter une balise `script` tout en bas du body.  Le script associé à la balise doit assigner le texte
"Travail Pratique III" à l'élément avec l'id `main-title`.


=== Sum de tous les nombres de 1 à $n$

#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur `#boutton-1`, et que l'`#input-1`
est une valeur numérique positive entière valide, il faut que le contenu 
de `#result-box-1` soit la somme de tous les nombres entiers positif entre 
0 et la valeur de `#input-1`.  Si la valeur de `#input-1` n'est pas une 
valeur numérique positive entière valide, alors le contenu 
de  `#result-box-1` doit être "Entrée invalide".

*Exemples*
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 1*],  [*result-box-1*], [*Explications*],
  [3], [6], [1 + 2 + 3],
  [10], [55],[1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10],
  [0], [0], [La sum vide est définit par zéro.],
  [allo], [Entrée invalide], [input 1 n'est pas un entier],
  [-12], [Entrée invalide], [input 1 est négatif]
)


*Données à utiliser:* Aucune.

=== Sum de tous les nombres de 1 à $n$ qui sont divisible par $k$

#points(none, 3, points-state) 

Lorsque l'utilisateur clique sur `#boutton-2`, et que l'`#input-1` et
 l'`#input-2` sont des valeurs numériques strictement positives entières valides, il faut 
 que le contenu de `#result-box-2` soit la somme de tous les nombres entiers positifs entre 
0 et la valeur de `#input-2` qui sont divisibles par `#input-1`.  Si la valeur de `#input-1` ou `#input-2` n'est pas une valeur numérique strictement positive entière valide, alors le contenu 
de  `#result-box-1` doit être "Entrée invalide".



*Exemples*
#table(
  columns: (auto, auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 1*], [*Input 2*], [*result-box-2*], [*Explications*],
  [1], [3], [6], [1 + 2 + 3],
  [2], [10], [30],[2 + 4 + 6 + 8 + 10], 
  [allo], [10], [Entrée invalide], [`#input-1` n'est pas un entier],
  [-12], [10], [Entrée invalide], [`#input-1` est négatif (incluant zéro)],
  [10], [allo], [Entrée invalide], [`#input-2` n'est pas un entier],
  [12], [-10], [Entrée invalide], [`#input-2` est négatif (incluant zéro)],
)

*Données à utiliser:* Aucune.


=== Pays d'une ville

#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur `#boutton-3` il faut que le contenu de 
`#result-box-3` soit le nom du pays de la ville spécifié par l'`#input-3`.
Si la ville n'est pas trouvée dans les données, le contenu de `#result-box-4`
doit être "ville non trouvée".

*Exemples*
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 3*],  [*result-box-3*], [*Explications*],
  [Oran], [Algerie], [Oran est situé en Algérie.],
  [Paris], [ville non trouvée], [Les données ne contiennent pas la ville de Paris.]
)

*Données à utiliser:* Villes.



=== Le nombre de villes d'un pays
#points(none, 3, points-state) 


Lorsque l'utilisateur clique sur `#boutton-4` il faut que le contenu de 
`#result-box-4` soit le nombre de villes dans les données qui sont dans 
le pays spécifié par l'`#input-4`.

*Exemples*
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 4*],  [*result-box-4*], [*Explications*],
  [Canada], [3], [Les trois villes sont Montreal, Toronto et Vancouver.],
  [France], [0], [Il n'y a aucune villes en France.]
)


*Données à utiliser:* Villes.



=== Population d'un pays
#points(none, 3, points-state) 

Lorsque l'utilisateur clique sur `#boutton-5` il faut que le contenu de 
`#result-box-1` soit la somme des populations des villes du pays 
spécifié par l'`#input-1`. Si aucune ville n'est trouvée pour dans le 
pays spécifié par l'utilisateur, le contenu de  `#result-box-1` doit être 
"0".

*Exemples*
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 1*],  [*result-box-1*], [*Explications*],
  [Canada], [5219553], [$1762949 + 2794356 + 662248$],
  [France], [0], [Il n'y a aucune villes en France.]
)


*Données à utiliser:* Villes.


=== Le plus grande ville d'un pays
#points(none, 4, points-state) 

Lorsque l'utilisateur clique sur `#boutton-6` il faut que le contenu de 
`#result-box-2` soit le nom de la ville la plus populeuse du pays 
spécifié par l'`#input-2`. Si aucune ville n'est trouvée pour dans le 
pays spécifié par l'utilisateur, le contenu de  `#result-box-2` doit être 
"aucune ville trouvée".

*Exemples*
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 2*],  [*result-box-2*], [*Explications*],
  [Canada], [Toronto], [Toronto est la ville avec le plus grand nombre d'habitant au Canada.],
  [France], [aucune ville trouvée], [Il n'y a aucune villes en France.]
)


*Données à utiliser:* Villes.


=== Pays d'origine
#points(none, 4, points-state) 

Lorsque l'utilisateur clique sur `#boutton-7` il faut que le contenu de 
`#result-box-3` soit le nom du pays d'origine de la personne 
spécifiée par l'`#input-3`. Si aucune personne n'est trouvée pour dans le 
nom spécifié par l'utilisateur, le contenu de  `#result-box-3` doit être 
"aucune personne trouvée".


*Exemples*
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 3*],  [*result-box-3*], [*Explications*],
  [Djaffar Bensetti], [Algerie], [Djaffar Bensetti est associé à la ville d'Oran qui est en Algerie.],
  [Marie Antoinette], [aucune personne trouvée], [Marie Antoinette ne fait pas partie de notre liste de personne.]
)

*Données à utiliser:* Villes et Personnes.

=== Nombre de personne dans un pays
#points(none, 4, points-state) 

Lorsque l'utilisateur clique sur `#boutton-8` il faut que le contenu de 
`#result-box-4` soit le nombre de personnes situé dans le pays 
spécifié par l'`#input-4`. Si aucune personne n'est trouvée pour dans le 
pays spécifié par l'utilisateur, le contenu de  `#result-box-4` doit être 
"0".


*Exemples*
#table(
  columns: (auto, auto, auto),
  inset: 10pt,
  align: horizon,
  [*Input 3*],  [*result-box-3*], [*Explications*],
  [Canada], [3], ["Maurice Vachon" est situé à Montreal au Canada, "Jane Munro" est situé à vancouver au Canada et "Eckhart Tolle" est situé à vancouver au Canada],
  [Mexique], [0], [Aucune personne est situé dans la ville de Mexico.],
  [France], [0], [La France ne fait pas partie de notre liste.]
)


*Données à utiliser:* Villes et Personnes.

<end>