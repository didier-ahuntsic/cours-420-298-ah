#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices, points, total-points

#let points-state = state("points", ());

#show: basic-footer
#show: doc => basic-header(
    "travail Pratique I | Site Web Statique", 
    extra-header-line: [Ce travail comporte #total-points(points-state, <end>) points.],
    doc
    )


#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


= Instructions

- Ce deuxième devoir doit être remis pour le 3 novembre 2023 à 18h00.
- Le  travail doit être fait individuellement.
- Le travail doit être remis sous la forme d'un projet 
  compréssé en format zip, par courriel au 
  didier.amyot\@collegeahuntsic.qc.ca ou par mio.
- Le travail doit contenir un petit README avec votre nom (5% sera enlevé sinon).
- Vous avez le droit à chat gpt mais chat gpt ne peut pas faire le
  travail à votre place.  _Copier chat gpt est du plagiat_.
- Vous devez être capable de défendre vos réponses dans un 
  examen oral.
- Ce devoir contient #total-points(points-state, <end>) points.


== Quoi remettre

Le dossier compréssé à remettre doit comprendre un README où vous écriverez
votre nom.  Tout le code doit se trouver dans un sous dossier `src`.  La 
page web principale doit se nommer `index.html`.  Ici est un exemple du résultat.

```
README.md
src/
├─ index.html
├─ script.js

```


== Bonne chance
Si vous avez des question, n'hésite pas à contacter votre enseignant au didier.amyot\@collegeahuntsic.qc.ca ou par mio.


= Énoncé

#rect(width: 100%, stroke: black)[
=== Truc

1. Lire et comprendre la question du point de vue de l'utilisateur.
2. Mettre la page web dans l'état attendu juste avant l'action.
3. Ouvrir l'inspecteur de chrome et essayer de faire l'exercice dans la console JavaScript.
4. Extraire la logique dans une fonction.
5. Mettre la fonction dans le fichier JavaScript (`script.js`).
6. Linker la fonction et le html avec un event approprié (`onclick`)
]


=== Exemples

Une implémentation du devoir est accessible au https://didier-ahuntsic.gitlab.io/cours-420-298-ah/examples/tp2.html.
Vous pouvez utiliser cette implémentation pour vous assurer de bien comprendre les questions.



#set heading(numbering: numbering-function, level: 1)

== Les Bases De La Programmation

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/tp2.html et modifier le code source.

=== Linker un script extérieur

#points(none, 1, points-state) 

Ajourter une balise `script` tout en bas du body.  Le script associé à la balise doit assigner le texte "Les Bases De La Programmation" à l'élément avec l'id `main-title`.


=== Copier Text
#points(none, 1, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-1`, il faut que le 
contenue de `#input-1` soit copié dans `#result-box-1`.


=== Valider si une valeure commence par un la lettre "a"
#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-2`, il faut que le contenue de 
`#result-box-2` soit "input-2 commence par la lettre 'a'" si la valeur de `#input-2`
commence par la lettre "a" et sinon "input-2 ne commence pas par la lettre 'a'".



=== Comparaison de longeurs
#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-3`, il faut que le contenue de 
`#result-box-3` soit "input-2 < input-3" si la valeur de `#input-2` contient strictement moins
de lettres que la valeur de `#input-3`.  Dans le cas contraire, le contenue de `#result-box-3`
doit être "input-2 >= input-3".


=== Comparaison de toutes les entrées
#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-4`, il faut que le contenue de 
`#result-box-4` soit "Tout est pareil" si les valeurs de `#input-1`, `#input-2`, `#input-3`
et `#input-4` sont toutes égales.  Dans le cas contraire, le contenue de 
`#result-box-4` doit être "Rien n'est pareil".

//-----

=== Arithmétique
#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-5`, il faut que le contenue de 
`#result-box-1` soit $(x - 32) #sym.times 1.8$ où $x$ est la valeur numérique de
`#input-1`.  Si `#input-1` n'est pas une valeure numérique, il faut que le contenue 
de `#result-box-1` soit "impossible de calculer".


=== Valeur Absolut
#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-6`, il faut que le contenue de 
`#result-box-2` soit la valeur absolut de la valeur numérique de `#input-2`.  Si `#input-2`
n'est pas une valeure numérique, il faut que le contenue 
de `#result-box-2` soit "impossible de calculer".


=== Produit
#points(none, 3, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-7`, il faut que le contenue de 
`#result-box-3` soit le produit de toutes les valeurs numériques de `#input-1`, `#input-2`,
`#input-3` et `#input-4`.  Si aucune input box a une valeur numérique, le contenue de 
`#result-box-3` doit être "1".



=== Somme des entrées possitives
#points(none, 3, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-8`, il faut que le contenue de 
`#result-box-4` soit la somme de toutes les valeurs numériques de `#input-1`, `#input-2`,
`#input-3` et `#input-4` qui sont possitives.  Si aucune input box a une valeur 
numérique possitive le contenue de `#result-box-3` doit être "0".

//---


=== Traduction
#points(none, 2, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-9`, il faut que le contenue de 
`#result-box-1` soit la traduction  de `#input-1` définit dans le dictionnaire plus bas.
Si la valeur de `#input-1` n'est pas une définit dans le dictionnaire, il faut que le 
contenue de `#result-box-1` soit "mot non trouvé".

#table(
  columns: (auto, auto),
  inset: 10pt,
  align: horizon,
  [*`#input-1` (Français)*], [*Traduction En Espérento*],
  [Lapin], [Kuniklo],
  [Lapinne], [Kuniklino],
  [Téléphone Cellulaire], [Poŝtelefono],
  [Ville], [Urbo],
)

=== Toutes les sorties | Est un grand nombre
#points(none, 3, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-10`, il faut que le contenue de toutes 
les boîtes de résultats changent selon la valeur de l'entré associé.  Par exemple,
le contenue de `#result-box-2` sera définit par la valeur de `#input-2`.

Le contenue de `#result-box-{n}` sera:

-  "Est un grand nombre" si `#input-{n}` est plus grand ou égale à 1000. 
-  "Est un petit nombre" si `#input-{n}` est scrictement plus petit que 1000. 
-  "N'est pas un nombre" si `#input-{n}` n'est pas une valeur numérique. 


=== Toutes les entrées | Est ordonné
#points(none, 3, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-11`, il faut que le contenue de 
`#result-box-3` soit "les données sont ordonnées" si `#input-1`, `#input-2`,
`#input-3` et `#input-4` sont toutes des valeurs numériques ordonnées en ordre croissant.

$ "#input-1" lt.eq  "#input-2" lt.eq  "#input-3" lt.eq  "#input-4" $

Dans le cas contraire, le contenue de `#result-box-3` doit être
"les données ne sont pas ordonnées".


=== Toutes les entrées et toutes les sorties

#points(none, 5, points-state) 

Lorsque l'utilisateur clique sur le bouton `#boutton-12`, il faut que le contenue de toutes 
les boîtes de résultats changent selon la valeur de l'entré associé.  Par exemple,
le contenue de `#result-box-2` sera définit par la valeur de `#input-2`.

Le contenue de `#result-box-{n}` sera:

-  "Maxima" si `#input-{n}` est la plus grande valeur numérique 
   parmis `#input-1`, `#input-2`, `#input-3`, et `#input-4`.
   Il peut y avoir plusieurs maximum.

-  "Minima" si `#input-{n}` est la plus petite valeur numérique 
   parmis `#input-1`, `#input-2`, `#input-3`, et `#input-4`.
   Il peut y avoir plusieurs miminum.

-  "Ni minima, ni maxima" si `#input-{n}` n'est ni un maxima ni minima
   parmis `#input-1`, `#input-2`, `#input-3`, et `#input-4`.
   Il peut y avoir plusieurs miminum.

-  Il n'y a pas de requis si `#input-{n}` n'est pas une valeur numérique.


Seules les valeurs numériques doivent être considérées dans ce numéro.





<end>