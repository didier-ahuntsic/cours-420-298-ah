#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Petite Trempette", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


Solutionnaire: https://github.com/didiercrunch/cours-420-298-ah/tree/main/petite_trempette


#set heading(numbering: numbering-function, level: 1)


=== Addition de deux nombre

Écrire une fonction `add` qui additionne deux nombre.  Se servir de la fonction `add` pour
additionner 103 et 198.



=== Multiplication de deux nombre

Écrire une fonction `multiply` qui additionne deux nombre.  Se servir de la fonction add pour
additionner 31 et 42.


=== Écrire une fonction qui convertie les Fahrenheit en Celsius

Écrire une fonction qui convertie un température de degré Fahrenheit à degré Celsius. La 
formule est ci-bas.  Se servir de cette fonction pour convertir 100 #sym.degree.f 
en #sym.degree.c.
$
  degree.c  = (degree.f - 32) / 1.8
$


=== Écrire une fonction qui convertie les degré Celsius en Kelvin

Écrire une fonction qui convertie un température de degré Celsius à Kelvin. La 
formule est ci-bas.  Se servir de cette fonction pour convertir 75 #sym.degree.c
en #sym.kelvin.
$
  kelvin  = degree.c +  273.15
$


=== Écrire une fonction qui convertie les degré Fahrenheit en Kelvin

Écrire une fonction qui convertie un température de degré Fahrenheit à Kelvin. Utiliser
les fonctions déjà écrites.  Se servir de cette fonction pour convertir 455 #sym.degree.f en 
Kelvin.



=== Valeure absolut d'un nombre 

Écrire une fonction `absolutValue` qui retourne la valeur absolut d'un nombre.  Se servir 
de cette fonction pour calculer la valeur absolut de $32$ et $-76$.


=== Est mineur

Écrire une fonction `isMinor` qui prend un age en paramêtre et retourne "la personne est mineur"
si l'age est plus petite que 18 ans, "la personne est mineur au états-unis" si la personne à moins de 21 ans et "la personne est majeur" sinon.

Essayer la fonction avec les ages 16, 20 et 56.

=== Peut prendre ça retraite

Écrire une fonction `canRetire` qui prend en argument deux nombres, l'age de la personne et 
sont nombre d'années d'expériences.  La fonction doit retourner "peut prendre ça retraite" si 
la somme de l'age et du nombre d'années d'expériences est plus grande que 80, "presque là" si 
la somme de l'age et du nombre d'années d'expériences est plus grande que 70 et "nope" sinon.

Essayer la fonction avec:

#table(
  columns: (auto, auto, auto),
  align: horizon,
  [*Age*], [*Années d'Expériences*], [*Retour*],
  [60], [25], ["peut prendre ça retraite"],
  [50], [25], ["presque là"],
  [40], [20], ["nope"],

)



=== Trivial fizzBuzz

Écrire une fonction `trivialFizzBuzz` qui prend un nombre en argument et retourn "fizz" si le 
nombre est strictement négatif, "buzz" si le nombre est strictement possitif et "fizzBuzz" si le nombre est zéro.

Essayer la fonction avec -13, 0 et 52.



=== Mini fizzBuzz

Écrire une fonction `miniFizzBuzz` qui prend un nombre en argument et retourn "fizz" si le 
nombre est divisible par 3, "buzz" si le nombre  est divisible par 5 et "fizzBuzz" si le nombre  est divisible par 15.

Il va falloir vous servir de l'opérateur remainder pour effecture ce numéro.  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Remainder 

Essayer la fonction avec 6 (fizz), 25 (buzz), 45 (fizzBuzz).