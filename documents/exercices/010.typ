#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Listes et Boucles", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a", ..nums);
    full.slice(full.position(regex("\\d+(\\.\w)?$")))
}


#set heading(numbering: numbering-function, level: 1)


== Créer un objet

=== Bière
Créer un objet qui représente la meilleure IPA au monde,
la Heady-Topper https://www.beeradvocate.com/beer/profile/46317/16814/ .

=== Livre
Créer un objet qui représente le livre, "JavaScript: The 
Good Part".  https://www.amazon.ca/dp/0596517742

=== Cours
Créer un objet qui représente le cours 420-298-ah
https://all-dressed-programming.com/courses/420-298-ah/

=== Album

Créer un objet qui représente l'album "If George 
Bush Was Cool".

https://vicspencer.bandcamp.com/album/if-george-bush-was-cool

=== Match de soccer

Créer un objet qui représente un match de soccer.
https://www.cfmontreal.com/competitions/mls-regular-season/2023/matches/miavsmtl-02-25-2023/

== Exercices techniques

=== getAttribute or default

Écrire une fonction `getAttribute` qui prend trois arguments.  Le  premier argument est un objet,
le deuxième une clé et le troisième une valeur par défaut.  La méthode doit retourner la valeur
de l'objet associée à la clé si la clé est dans l'objet; sinon la valeur par défaut.


```js
console.log(getAttribute({age: 100}, "age", 0)) // 100
console.log(getAttribute({name: "claudette"}, "age", 0)) // 0
```

=== Dissoc

Écrire une fonction `dissoc` qui prend un objet, et une clé en argument.
La fonction doit retourner un nouvel objet  possédant tout les couples clé/valeur
de l'objet passé en argument sauf celui associé à la clé passé en arguments.

```js
console.log(dissoc({age: 100, name: "claudette"}, "age")) // {name: "claudette"}
console.log(dissoc({age: 100, name: "claudette"}, "country")) // {age: 100, name: "claudette"}
```

=== Inversion

Écrire une fonction `invert` qui prend un objet en argument et qui retourne
un nouvel objet qui inverse la relation clé valeur.

```js
console.log(invert({boris: "plombier", didier: "enseignant"})) // {enseignant: "didier", plombier: "boris"}
```

=== GetIn

Écrire une fonction `getIn` qui prend un objet et un array en argument. 
La fonction doit retourner la valeur associé au chemin par l'array.

```js
getIn({person: {name: "didier"}}, ["person", "name"]) // "didier"

getIn({person: {name: "didier"}}, ["person", "age"]) // undefined
```

==  Recherche dans une liste d'objets

Pour répondre aux questions plus bas, vous devez vous utiliser
les données ci-bas.

- https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/books.json
- https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/prices.json

=== Toutes les BDs

Trouver tous les livres qui sont des BDs.

=== Toutes les grosses BDs

Trouver tous les livres qui sont des BDs de plus de 100 pages.

=== Toutes les livres

Trouver tous les livres qui sont des BDs de plus de 100 pages.

=== Livre le plus cher
Quel est le titre du livre le plus cher?

=== Livre le moins cher
Quel est le titre du livre le moins cher?

=== Profit Moyen 
Quel est le profit moyen lors de la vente d'un livre.

=== Plus grand profit 
Quel est le titre du livre qui rapport le plus de profit.
