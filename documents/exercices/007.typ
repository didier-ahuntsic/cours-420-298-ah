#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Les Bases Du JavaScript", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.a.i", ..nums);
    full.slice(full.position(".") + 1)
}





#let link-palindome = link("https://fr.wikipedia.org/wiki/Palindrome")[palindrome]
#let link-carre-parfait = link("https://fr.wikipedia.org/wiki/Carr%C3%A9_parfait")[carré parfait]
#let link-fibb = link("https://en.wikipedia.org/wiki/Fibonacci_sequence")[nombres de Fibonacci]
#let link-us-timeformat = link("https://fr.wikipedia.org/wiki/Syst%C3%A8me_horaire_sur_12_heures")[heure de format américaine]
#let link-typst = link("https://typst.app")[typst]

#rect(width: 100%, stroke: black)[
=== Truc

1. Lire et comprendre la question du point de vue de l'utilisateur.
2. Mettre la page web dans l'était attendu juste avant l'action.
3. Ouvrir l'inspecteur de chrome et essayer de faire l'exercice dans la console JavaScript.
4. Extraire la logique dans une fonction
5. Mettre la fonction dans le fichier javascript (`script.js`).
6. linker la fonction et le html avec un event approprié (`onclick`)

]

#set heading(numbering: numbering-function, level: 1)

== Input Output

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/excel.html et modifier le code source.

=== Linker un script extérieur

Ajourter une balise `script` tout en bas du body.  La script associé à la balise doit assigner le text "Input Output" à l'élément avec l'id `main-title`.


=== Copier text

L'objectif de cet exercice est de copier le text d'une entré dans une sortie.

Lorsque l'utilisateur clique sur le bouton `#boutton-1`, il faut que le 
contenue de `#input-1` soit copié dans `#result-box-1`.


=== Copier text

L'objectif de cet exercice est de comparer deux entrés de l'utilisateurs.

Lorsque l'utilisateur clique sur le bouton `#boutton-2`, il faut que le contenue de `#result-box-2` 
soit `input-1 = input-2` si la valeur de `#input-1` est la même que `#input-2` et
 `input-1 != input-2` sinon.

=== Est un nombre

L'objectif de cet exercice est de convertir l'entré d'un utilisateur en nombre.

Lorsque l'utilisateur clique sur le bouton `#boutton-3`, il faut que le contenue de 
`#result-box-3` soit `input-3 est un nombre!` si la valeur de `#input-3` est 
un nombre et `input-3 n'est pas un nombre` sinon.

=== Travailler avec un nombre

L'objectif de cet exercice est de Travailler avec l'entré d'un utilisateur en nombre.

Lorsque l'utilisateur clique sur le bouton `#boutton-4`, il faut que le contenue de 
`#result-box-4` soit `input-4 est plus grand que 1000` si la valeur de 
`#input-4` est plus grande que 1000, `input-4 est plus plus petit 1000` si `#input-4` 
est plus petit que 1000 et `#input-4 n'est pas un nombre` si `#input-4` n'est pas un
nombre.


== Indirections

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/excel.html et modifier le code source.

=== Linker un script extérieur

Ajourter une balise `script` tout en bas du body.  La script associé à la balise doit assigner 
le text "Indirections" à l'élément avec l'id `main-title`.

=== Concaténation 

Lorsque l'utilisateur clique sur le bouton `#boutton-1`, il faut que le contenue de 
`#result-box-1` montre la concaténation de la valeur de tous les input box `#input-1`, `#input-2`, 
`#input-3` et `#input-4`.

=== Indirection I

Lorsque l'utilisateur clique sur le bouton `#boutton-2`, il faut que le contenue de 
`#result-box-2` montre la valeur du input box `#input-{id}` ou `id` est la valeur 
de l'input box `#input-2`.

=== Indirection II

Lorsque l'utilisateur clique sur le bouton `#boutton-3`, il faut que le contenue de 
`#result-box-3` montre le texte contenue dans la balise `#button-{id}` ou `id` est la valeur 
de l'input box `#input-3`.

=== Indirection III

Lorsque l'utilisateur clique sur le bouton `#boutton-4`, il faut que le contenue de 
`#result-box-4` montre le texte contenue ou la valeur de la balise définit par `#{id}`
où `id` est la valeur  de l'input box `#input-4`.


== Los Statisticos

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/excel.html et modifier le code source.

=== Linker un script extérieur

Ajourter une balise `script` tout en bas du body.  La script associé à la balise doit assigner le text "Los Statisticos" à l'élément avec l'id `main-title`.

=== Nombre de valeur 

Lorsque l'utilisateur clique sur le bouton `#boutton-1`, il faut que le contenue de 
`#result-box-1` montre le nombre de input box parmis `#input-1`, `#input-2`, `#input-3`, 
`#input-4` qui contiennent des valeurs numériques.


=== Moyenne

Lorsque l'utilisateur clique sur le bouton `#boutton-2`, il faut que le contenue de 
`#result-box-2` montre la moyenne de input box parmis `#input-1`, `#input-2`, `#input-3`, 
`#input-4` qui contiennent des valeurs numériques.  S'il est impossible de calculer la moyenne,
`#result-box-2` devrait montrer "impossible de calculer une moyenne d'une liste vide.".


=== Écart-type

Lorsque l'utilisateur clique sur le bouton `#boutton-3`, il faut que le contenue de 
`#result-box-3` montre l'écart-type de input box parmis `#input-1`, `#input-2`, `#input-3`, 
`#input-4` qui contiennent des valeurs numériques. S'il est impossible de calculer l'écart-type,
`#result-box-3` devrait montrer "impossible de calculer l'écart-type d'une liste vide.".


== Le Respectable Rectangle

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/rectangle.html 
et modifier le code source.

=== Linker un script extérieur

Ajourter une balise `script` tout en bas du body.  La script associé à la balise doit assigner le text 
"Le Respectable Rectangle" à l'élément avec l'id `main-title`.


=== Hauteur du rectagle

Lorsque l'utilisateur clique sur le bouton `#change`, il faut que la heuteur du rectable `#rect`
prenne la valeur de l'input box `#height`.

=== Largeur du rectagle

Lorsque l'utilisateur clique sur le bouton `#change`, il faut que la largeur du rectable `#rect`
prenne la valeur de l'input box `#width`.

=== Air du rectagle

Lorsque l'utilisateur clique sur le bouton `#change`, il faut que la valeur écrite dans `#area`
prenne la valeur de l'aire du rectagle.


=== Périmetre du rectagle

Lorsque l'utilisateur clique sur le bouton `#change`, il faut que la valeur écrite dans `#perimeter`
prenne la valeur du Périmetre du rectagle.


==  الخط

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/line.html 
et modifier le code source.

Le but de cet exercice est de pratiquer les itérations.

=== Linker un script extérieur

Ajourter une balise `script` tout en bas du body.  La script associé à la balise doit assigner le text 
" الخط" à l'élément avec l'id `main-title`.


=== Tous les ids

Lorsque l'utilisateur clique sur le bouton `#button-1`, il faut que le contenue de `#result-box-1` contienne
la liste de tous les id des 8 inputboxs.


=== Mot non vide

Lorsque l'utilisateur clique sur le bouton `#button-2`, il faut que le contenue de `#result-box-2` montre
le nombre d'input box non vide parmis les 8 input boxs.

=== Nombre de mot qui commence par la letter "A"

Lorsque l'utilisateur clique sur le bouton `#button-3`, il faut que le contenue de `#result-box-3` montre
le nombre de mot qui commence par la letter "A" parmis les 8 input boxs.

=== Ajouter un contour rouge

Lorsque l'utilisateur clique sur le bouton `#button-4`, il faut que le contour (border) de chaque
input box vide devienne rouge.


== Échec

Pour ce numéro, vous devez copier  https://didier-ahuntsic.gitlab.io/cours-420-298-ah/bases/chess.html 
et modifier le code source.

#let chessboard(color: none) = {
    rect(width: 128pt, height: 128pt, inset:  0pt)[
        #for row in range(8) {
            for col in range(8){
                place(
                        top + left,
                        dx: 16pt * col,
                        dy: 16pt * row,
                        square(
                            width: 16pt, 
                            stroke: black,
                            fill:  if color == none { none } else { color(row, col) },
                        ),
                    )
            }
        }
    ]
}

=== Linker un script extérieur

Ajourter une balise `script` tout en bas du body.  La script associé à la balise doit assigner le text 
"Échec" à l'élément avec l'id `main-title`.


=== Dessinez un bonhomme sourrir.

Lorsque l'utilisateur clique sur le bouton `#button-1`, il faut qu'on bonhomme sourrir se dessine 
sur l'échiquier.  Vous pouvez dessiner le bonhomme sourrir en choisisant les cases manuellement. 

=== Le bouton reset

Lorsque l'utilisateur clique sur le bouton `#reset`, il faut que toutes les cases reviennent blanches.


=== Moitier Haut

Lorsque l'utilisateur clique sur le bouton `#button-2`, il faut que toutes les cases de la moitié 
du haut deviennent noir.

#chessboard(color: (row, col) => if row <= 4 {rgb("#888888")}  else {white})


=== Moitier Bas

Lorsque l'utilisateur clique sur le bouton `#button-3`, il faut que toutes les cases de la moitié 
du bas deviennent noir.

#chessboard(color: (row, col) => if row >= 4 {rgb("#888888")}  else {white})

=== Moitier Gauche

Lorsque l'utilisateur clique sur le bouton `#button-4`, il faut que toutes les cases de la moitié 
gauche deviennent noir.

#chessboard(color: (row, col) => if col <= 4 {rgb("#888888")}  else {white})

=== Moitier Droite

Lorsque l'utilisateur clique sur le bouton `#button-5`, il faut que toutes les cases de la moitié 
droite deviennent noir.

#chessboard(color: (row, col) => if col >= 4 {rgb("#888888")}  else {white})

=== Diagonale du Haut

Lorsque l'utilisateur clique sur le bouton `#button-6`, il faut que toutes les cases se trouvant au 
dessus de la diagonale deviennent noir.

#chessboard(color: (row, col) => if row <= col {rgb("#888888")}  else {white})

=== Diagonale du Bas

Lorsque l'utilisateur clique sur le bouton `#button-6`, il faut que toutes les cases se trouvant au 
dessous de la diagonale deviennent noir.

#chessboard(color: (row, col) => if row >= col {rgb("#888888")}  else {white})


=== Sur les Diagonales

Lorsque l'utilisateur clique sur le bouton `#button-7`, il faut que toutes les cases se trouvant sur les diagonale deviennent noir.

#chessboard(color: (row, col) => if row == col or row == (7 - col) {rgb("#888888")}  else {white})


=== Lignes et colonnes

Lorsque l'utilisateur clique sur le bouton `#button-8`, il faut que toutes les cases se trouvant
sur la troisième et sixème ligne ou colonnes deviennent noir.

#chessboard(color: (row, col) => if row == 2 or row == 5 or col == 2 or col == 5 {rgb("#888888")}  else {white})


=== Échiquier Aléatoire 

Lorsque l'utilisateur clique sur le bouton `#button-9`, les cases de l'échiquier doit devenir noir de
manière aléatoire.


#chessboard(color: (row, col) => if calc.odd(calc.rem(1451 * row + 881 * col, 83))  {rgb("#888888")}  else {white})


=== Échiquier 

Lorsque l'utilisateur clique sur le bouton `#button-10`, les cases de l'échiquier doivent ressembler au
dessin ci-base.


#chessboard(color: (row, col) => 
                    if (0, 1, 6, 7).contains(col) and (0, 1, 6, 7).contains(row)  or 
                        not (0, 1, 6, 7).contains(col) and not (0, 1, 6, 7).contains(row) 
                    {rgb("#888888")}  else {white})


=== Échiquier 

Lorsque l'utilisateur clique sur le bouton `#button-11`, toutes les cases de l'échiquier où 
l'index est un nombre premier doivent devenir noir.

#let is-prime(n) = {
    if n == 1 {
        return false
    }
   for i in range(2, n){
    if calc.rem(n, i) == 0{
        return false
    }
   }
   return true
}

#chessboard(color: (row, col) => 
                    if is-prime(col + 8 * row + 1)
                    {rgb("#888888")}  else {white})


=== Vrai Échiquier

Lorsque l'utilisateur clique sur le bouton `#button-12`, l'échiquier doit ressembler à un vrai échiquier.


#chessboard(color: (row, col) => if (calc.odd(row) and calc.odd(col)) or(not calc.odd(row) and not calc.odd(col))  {rgb("#888888")}  else {white})


