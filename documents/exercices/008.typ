#import "../common.typ": basic-footer, basic-header, answer-line, code-block-answer, answer-choices

#show: basic-footer
#show: doc => basic-header("Exercices | Listes et Boucles", doc)
#let numbering-function(.. nums) = {
    let full = numbering("1.1.1.a", ..nums);
    full.slice(full.position(regex("\\d+$")))
}


Solutionnaire: https://github.com/didiercrunch/cours-420-298-ah/blob/main/exo-008/script.js


#set heading(numbering: numbering-function, level: 1)


=== Fonction range 

Écrire une fonction `range` qui prend un nombre $n$ en argument et qui retourne 
tous les nombre de $0$ jusqu'à $n-1$.

```javascript
let from0To10 = range(10);
console.log(from0To10); // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

=== Imprimer une liste

Écrire une fonction `printEachElement` qui imprime chaque élément d'une liste.  Se 
servir de la fonction `printEachElement` pour imprimer tous les éléments de la 
liste `["bonjour", "mon", "poussin"]`.


=== Prendre un élément 

Écrire une fonction `getElementAt` qui prend une liste et un index en argument.  La fonction `getElementAt`
doit retourner la valeur de la liste à l'index. 

```javascript 
let lst = [10, 18, 15];
let ret = getElementAt(lst, 1);
console.log(ret) // 18
```

=== Prendre le premier element 

Écrire une fonction `getFirstElement` qui prend une liste et retourne le premier élément de la liste.

```javascript 
let lst = [10, 18, 15];
let ret = getFirstElement(lst);
console.log(ret) // 10
```


=== Prendre le deuxième element 

Écrire une fonction `getSecondElement` qui prend une liste et retourne le deuxième élément de la liste.

```javascript 
let lst = [10, 18, 15];
let ret = getSecondElement(lst);
console.log(ret) // 18
```


=== Sauf le premier

Écrire une fonction `restList` qui prend une liste et retourne tous les éléments de la liste sauf le premier.

```javascript 
let lst = [10, 18, 15];
let ret = restList(lst);
console.log(ret) // [18, 15]
```


=== Sauf le dernier

Écrire une fonction `butLast` qui prend une liste et retourne tous les éléments de la liste sauf le dernier.

```javascript 
let lst = [10, 18, 15];
let ret = butLast(lst);
console.log(ret) // [10, 18]
```


=== Prendre $n$ éléments

Écrire une fonction `takeN` qui prend une liste, et un nombre $n$ et qui retourne les $n$ premiers éléments de la liste.
Si la liste contient moins de $n$ éléments, tous les éléments de la liste doivent être retournés.

```javascript 
let lst = [3, 4, 5, 6, 7];

console.log(takeN(ret, 3))   // [3, 4, 5]
console.log(takeN(ret, 100)) // [3, 4, 5, 6, 7]
```


=== Ne pas prendre $n$ éléments

Écrire une fonction `dropN` qui prend une liste, et un nombre $n$ et qui retourne tous les éléments de la liste sauf les $n$ premiers.
Si la liste contient moins de $n$ éléments,la liste vide doit être retournés.

```javascript 
let lst = [3, 4, 5, 6, 7];
let ret = butLast(lst);

console.log(dropN(ret, 3))   // [6, 7]
console.log(dropN(ret, 100)) // []
```


=== Sommer tous les éléments d'une liste 

Écrire une fonction `sumListElements` qui prend une liste de nombre et somme 
tous les éléments ensemble.  Utiliser la fonction `sumListElements` pour 
trouver la somme de tous les éléments de la liste `[8, 2, 19, 5, 6]`.


```javascript
let lst = [8, 2, 19, 5, 6];
console.log(sumListElements(lst)); // 40
```


=== Multiplier tous les éléments d'une liste 

Écrire une fonction `multiplyListElements` qui prend une liste de nombres et multiplie
tous les éléments ensemble.  Utiliser la fonction `multiplyListElements` pour 
trouver le produit de tous les éléments de la liste `[8, 2, 19, 5, 6]`.


```javascript
let lst = [8, 2, 19, 5, 6];
console.log(multiplyListElements(lst)); // 9120
```

=== copier une liste

Écrire une fonction `copyList` qui prend une liste en argument et qui retourne une nouvelle 
liste qui contient exactement les mêmes éléments.

```javascript 
let lst = [8, 2, 19, 5, 6];
let ret = copyList(lst) ;
lst.push(50); // modifier la liste d'entré 
              // ne devrait pas modifier la liste de sortie

console.log(ret); // [8, 2, 19, 5, 6]
```


=== isIn

Écrire une fonction `isIn` qui prend une liste et un élément en argument et retourne `true` si 
l'élément est dans la liste et `false` dans le cas contraire.  Utiliser `isIn` pour vérifier 
que `5` est dans la liste `[8, 6, 5, 19, 10]` et que `7` n'est pas dans la liste 
`[8, 6, 5, 19, 10]`.


```javascript
let lst = [8, 6, 5, 19, 10];
let element = 5;
console.log(isIn(lst, element)); // true


element = 7;
console.log(isIn(lst, element)); // false
```


=== Nombres possitifs

Écrire une fonction `extractPossitiveNumbers` qui prend une liste de nombres et 
retourne une nouvelle liste qui contient les éléments positifs de la liste passés en 
argument.

```javascript
let lst = [10, -10, 2, -4, -5, 5]
let ret = extractPossitiveNumbers(lst);
console.log(ret) // [10, 2, 5]
```

